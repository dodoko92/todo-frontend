import React from 'react';
import { shallow, mount } from 'enzyme';
import App from './App';

it('renders without crashing', () => {
  const component = shallow(<App />);

  expect(component.getElements()).toMatchSnapshot();
});


describe('<App />', () => {
  let setTodos;

  beforeEach(() => {
    setTodos = jest.fn();
    const handleChangeTodos = jest.spyOn(React, "useState");
    handleChangeTodos.mockImplementation(todos => [todos, setTodos]);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("should update todos in state", () => {
    const component = mount(<App />);
    component.find('input').simulate('change', { target: { value: [{ text: 'buy some milk' }] } });

    expect(setTodos).toHaveBeenCalledWith([{ 'text': 'buy some milk' }]);
  });


})

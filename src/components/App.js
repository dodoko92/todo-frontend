import React, { useEffect } from 'react';
import './App.css';
import Todo from './Todo';
import TodoInput from './TodoInput';

import TodoClient from './../clients/TodoClient';

function App() {

  const [todos, setTodos] = React.useState([]);


  useEffect(
    () => {
      const todoClient = new TodoClient({ host: `${process.env.REACT_APP_API_URL}` });
      todoClient.getTodos().then(
        (data) => setTodos(data)
      )
    }, []
  );

  const addTodo = text => {
    const todoClient = new TodoClient({ host: `${process.env.REACT_APP_API_URL}` });
    if (!text) return;
    const newTodos = [...todos, { text }];
    todoClient.createTodo({ text: text }).then(
      (data) => setTodos(newTodos)
    );
  };


  return (
    <div className="App">
      <TodoInput addTodo={addTodo} />
      <h1>Todo List</h1>
      <ul>
        {todos.map((todo, index) => (
          <Todo
            key={index}
            index={index}
            text={todo.text}
          />
        ))}
      </ul>
    </div>
  );
}

export default App;

import React from 'react';
import { shallow } from 'enzyme';
import Todo from './Todo';

it('renders without crashing', () => {
  const component = shallow(<Todo />);

  expect(component.getElements()).toMatchSnapshot();
});
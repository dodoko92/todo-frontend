import React from 'react';

function TodoInput({ addTodo }) {
  const [value, setValue] = React.useState("");

  const handButton = () => {
    addTodo(value);
    setValue("");
  };

  return (
    <div>
      <input
        type="text"
        id="input-text"
        value={value}
        onChange={e => setValue(e.target.value)}
      />
      <button onClick={handButton}>Add Todo</button>
    </div>
  );
}



export default TodoInput;
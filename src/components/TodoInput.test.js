import React from 'react';
import { shallow, mount } from 'enzyme';
import TodoInput from './TodoInput';

it('renders without crashing', () => {
  const component = shallow(<TodoInput />);

  expect(component.getElements()).toMatchSnapshot();
});


describe('<TodoInput />', () => {
  let setValue;

  beforeEach(() => {
    setValue = jest.fn();
    const handleChangeValue = jest.spyOn(React, "useState");
    handleChangeValue.mockImplementation(value => [value, setValue]);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("should create a value in state", () => {
    const component = mount(<TodoInput />);
    component.find('input').simulate('change', { target: { value: 'buy some milk' } });

    expect(setValue).toHaveBeenCalledWith('buy some milk');
  });

  it("should call addTodo when click button", () => {
    const addTodo = jest.fn();

    const component = mount(<TodoInput addTodo={addTodo} />);
    
    component.find('button').simulate('click');
    expect(addTodo).toHaveBeenCalled();
  });

})

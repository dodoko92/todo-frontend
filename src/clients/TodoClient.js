import axios from 'axios';

class TodoClient {
  constructor(options) {
    this.host = options.host;
  }

  getTodos() {
    const headers = { "Accept": "application/json" };

    return axios({
      url: `${this.host}/todos`,
      method: 'GET',
      headers
    })
      .then(function (response) {
        return response.data;
      });
  }

  createTodo(todo) {
    const headers = { "Accept": "application/json" };
    return axios({
      url: `${this.host}/todos`,
      method: 'POST',
      headers,
      data: todo
    })
      .then(function (response) {
        return response.data;
      });
  }
}

export default TodoClient;

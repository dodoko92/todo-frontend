import * as Pact from "@pact-foundation/pact";


const todoOne = {
  text: Pact.Matchers.somethingLike("Todo 1")
};

const todoTwo = {
  id: Pact.Matchers.somethingLike(1),
  text: Pact.Matchers.somethingLike("Todo 1")
};

export const todoClientFixtures = {
  allTodosResponse: Pact.Matchers.somethingLike(Pact.Matchers.eachLike(todoTwo)),
  createTodoInput: Pact.Matchers.somethingLike(todoOne),
  todoResponse: Pact.Matchers.somethingLike(todoTwo),
};
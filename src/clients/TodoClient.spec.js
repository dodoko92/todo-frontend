import { todoClientFixtures } from './fixtures/todo-client';
import TodoClient from './TodoClient';


describe('Todo Client', () => {

  const todoClient = new TodoClient({ host: `http://localhost:${global.port}` });


  describe('createTodo(todo)', () => {

    beforeEach((done) => {
      global.provider.addInteraction({
        uponReceiving: 'a request to create a new todo',
        withRequest: {
          method: 'POST',
          path: '/todos',
          body: todoClient.createTodoInput,
          headers: { 'Accept': 'application/json' }
        },
        state: 'create todo',
        willRespondWith: {
          status: 201,
          headers: { 'Content-Type': 'application/json; charset=utf-8' },
          body: todoClientFixtures.todoResponse
        }
      }
      ).then(() => done()).catch((err) => catchAndContinue(err, done));
    });

    it('sends a request according to contract', async () => {
      const result = await todoClient.createTodo(todoClientFixtures.createTodoInput);
      expect(result).toEqual({ id: 1, text: "Todo 1" });
    });

  });

  describe('getTodos()', () => {

    beforeEach((done) => {
      global.provider.addInteraction({
        uponReceiving: 'a request to get all todos',
        withRequest: {
          method: 'GET',
          path: '/todos',
          headers: { 'Accept': 'application/json' }
        },
        state: 'get todos',
        willRespondWith: {
          status: 200,
          headers: { 'Content-Type': 'application/json; charset=utf-8' },
          body: todoClientFixtures.allTodosResponse
        }
      }
      ).then(() => done()).catch((err) => catchAndContinue(err, done));
    });

    it('get data from contract', async () => {
      const result = await todoClient.getTodos();
      expect(result).toEqual([{ id: 1, text: "Todo 1" }]);
    });

  });

});
import puppeteer from 'puppeteer'

const appUrlBase = process.env.TEST_APP_URL

let browser
let page

beforeAll(async () => {
  browser = await puppeteer.launch({
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox'
    ]
  })
  page = await browser.newPage()
})

describe('Todo App Acceptence Test', () => {

  test('Adding a TODO item', async () => {
    await page.goto(`${appUrlBase}`, {
      waitUntil: 'networkidle2',
    })

    const ulEl = await page.evaluate(() => {
      return document.querySelector('ul').innerHTML
    })
    expect(ulEl).toEqual('')

    //Enter value in the input
    await page.type("#input-text", "buy some milk");

    await page.evaluate(() => {
      document.querySelector('button').click()
    })

    const ulEl2 = await page.evaluate(() => {
      return document.querySelector('ul').innerHTML
    })
    expect(ulEl2).toEqual('<li>buy some milk</li>')

  })

})

afterAll(() => {
  browser.close()
})
let publisher = require('@pact-foundation/pact-node');
let path = require('path');

let opts = {
  pactFilesOrDirs: [path.resolve(process.cwd(), 'pacts')],
  pactBroker: 'https://todoapptest.pactflow.io',
  pactBrokerToken: process.env.PACT_BROKER_TOKEN,
  consumerVersion: process.env.CI_COMMIT_SHORT_SHA,
  tags: [process.env.CI_COMMIT_REF_NAME],
  environments: ["production"]
};

publisher.publishPacts(opts).then(() => console.log("Pacts successfully published"));

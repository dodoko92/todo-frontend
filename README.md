# Todo React Frontend Example with A-TDD Development

## Requirements
- npm 7.20.3
- node v16.6.2

## How to Run Development
Before you start
```bash
$ npm install
```
then
```bash
$ npm start
```

## Test Cases
### Run only Acceptance Test Case
Acceptance Test Requrires a Running Instance 

```bash
$ npm run test -- Acceptance
```

### Run Components and Unit Test Cases
```bash
$ npm run test -- components
```

### Run Contract Test Cases
```bash
$ npm run test:pact -- Client
```

### Publish Pacts to Broker
```bash
$ npm run publish:pact
```

## Run on Docker
Before you build image build app first
```bash
$ npm run build
```

### How to build Docker image
```bash
$ docker build -t todo-frontend:latest .
```

### How to run built image
```bash
$ docker run -p 80:80 todo-frontend
```

## References
* [Acceptance UI Testing with Puppeteer](https://github.com/puppeteer/puppeteer])

* [Puppeteer API DOC](https://pptr.dev/#?product=Puppeteer&version=v11.0.0&show=outline)

* [TDD and Unit Testing with React Enzyme and Jest](https://www.toptal.com/react/tdd-react-unit-testing-enzyme-jest)

* [Digitalocean Build Todo App with React Hooks](https://www.digitalocean.com/community/tutorials/how-to-build-a-react-to-do-app-with-react-hooks)

* [Airbnb Enzyme](https://airbnb.io/projects/enzyme/)

* [Pact.js Api Contract Tests](https://github.com/pact-foundation/pact-js#http-api-testing)

* [Consumer Driven Contracts using Pact](https://blog.scottlogic.com/2017/01/10/consumer-driven-contracts-using-pact.html)

* [Pact React Consumer](https://github.com/ang3lkar/pact-react-consumer)

* [React in Docker with Nginx, built with multi-stage Docker builds, including testing](https://tiangolo.medium.com/react-in-docker-with-nginx-built-with-multi-stage-docker-builds-including-testing-8cc49d6ec305)

* [GitLab CI/CD example with a dockerized ReactJS App](https://dev.to/christianmontero/gitlab-ci-cd-example-with-a-dockerized-reactjs-app-1cda)

* [Implementing a consumer-driven contract testing workflow with Pact broker and GitLab CI](https://blog.codecentric.de/en/2020/02/implementing-a-consumer-driven-contract-testing-workflow-with-pact-broker-and-gitlab-ci/)